//
//  TabButtonView.swift
//  Trial Tab Controller
//
//  Created by Orhun Dündar on 15.01.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

protocol TrialButton {
    func buttonPressed(count:Int)
}

class TabButtonView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var testNumberLAbel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var isTestAcceptable:Bool = false
    var testNumber:Int = 0
    var delegate:TrialButton!
    
    init(frame: CGRect,isTestAcceptable: Bool, testNumber: Int) {
        super.init(frame: frame)
        self.isTestAcceptable = isTestAcceptable
        self.testNumber = testNumber
        self.commonInit()
        initView()
    }

    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
        fatalError("NSCoding not supported")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("TabButtonView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }
    
    func initView(){
        //Init button view
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = UIColor.gray.cgColor
        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
        
        // set test correction image
        if isTestAcceptable {
            self.button.setImage(UIImage(named: "greenCheck"), for: .normal)
        }
        else{
            self.button.setImage(UIImage(named: "redClose"), for: .normal)
        }
        
        //set test number
        if testNumber == -1 {
            self.testNumberLAbel.text = "Result"
            self.button.setImage(nil, for: .normal)
        }
        else{
            self.testNumberLAbel.text = String(self.testNumber)
        }
        
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        delegate.buttonPressed(count: testNumber)
        self.contentView.backgroundColor = UIColor(red: 0, green: 0.612, blue: 0.922, alpha: 1)
        self.testNumberLAbel.textColor = .white
    }
    
    func resetButton(){
        self.contentView.backgroundColor = UIColor.white
        self.testNumberLAbel.textColor = .gray
    }
    
}
