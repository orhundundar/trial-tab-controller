//
//  Measurement.swift
//  Trial Tab Controller
//
//  Created by Orhun Dündar on 20.01.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation
import ObjectMapper

//Measurement Weather
public struct MeasurementWeather:Mappable {
    public var temperature : Double?
    public var humidity : Double?
    public var pressure : Double?
    
    init() {}
    
    public init?(map: Map){}
    
    mutating public func mapping(map: Map) {
        temperature <- map["temperature"]
        humidity <- map["humidity"]
        pressure <- map["pressure"]
    }
}

//Measurement Location
public struct MeasurementLocation:Mappable {
    public var latitude : Double?
    public var longitude : Double?
    public var altitude : Double?
    public var address : String?
    
    init() {}
    
    public init?(map: Map){}
    
    mutating public func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        altitude <- map["altitude"]
        address <- map["address"]
    }
}

//Measurement
public struct SMeasurement:Mappable {
    public var _id : String?
    public var clientTestId : String!
    public var patientId : String?
    public var deviceSerialNumber : String?
    public var isPost : Bool?
    public var testType : String?
    public var testQuality : String!
    public var testDate: String?
    public var weather: MeasurementWeather?
    public var location: MeasurementLocation?
    public var predictedType:String?
    public var patientAge:Int?
    public var patientWeight:Double?
    public var patientHeight:Double?
    public var tests : [MeasurementTest]!
    public var preTestId:String?
    public var postTestId:String?
    public var healthCompanyId:String?
    public var healthCompanyUserId:String?
    
    init() {}
    
    public init?(map: Map){}
    
    mutating public func mapping(map: Map) {
        _id <- map["_id"]
        clientTestId <- map["clientTestId"]
        patientId <- map["patientId"]
        deviceSerialNumber <- map["deviceSerialNumber"]
        isPost <- map["isPost"]
        testType <- map["testType"]
        testQuality <- map["testQuality"]
        testDate <- map["testDate"]
        weather <- map["weather"]
        location <- map["location"]
        predictedType <- map["predictedType"]
        tests <- map["tests"]
        patientAge <- map["patientAge"]
        patientWeight <- map["patientWeight"]
        patientHeight <- map["patientHeight"]
        preTestId <- map["preTestId"]
        postTestId <- map["postTestId"]
        healthCompanyId <- map["healthCompanyId"]
        healthCompanyUserId <- map["healthCompanyUserId"]
    }
}


//Measurement Test
public struct MeasurementTest:Mappable {
    public var flowData : [Float]!
    public var parameters : [MeasurementTestParameter]!
    public var atsErrorCodes : [String]!
    
    init() {}
    
    public init?(map: Map){}
    
    mutating public func mapping(map: Map) {
        flowData <- map["flowData"]
        parameters <- map["parameters"]
        atsErrorCodes <- map["atsErrorCodes"]
    }
}


//Measurement Test Parameter
public struct MeasurementTestParameter:Mappable {
    public var parameterType : String!
    public var parameterValue : Float = 0
    public var parameterPredicted : Float? = 0
    public var parameterPercent: Float = 0
    public var parameterPercentColor:UIColor?
    public var parameterPercentFeedback:String?
    public var parameterLLN:Float?
    public var zScore:Float?
    
    init( _ type:String, _ value:Float, _ predicted:Float?) {
        parameterType = type
        parameterValue = checkInfinite(value) ?? 0
        parameterPredicted = checkInfinite(predicted)
        calculatePercentValues()
    }
    
    init( _ type:String, _ value:Float, _ predicted:Float?, _ LLN:Float?, _ zScore:Float?) {
        parameterType = type
        parameterValue = checkInfinite(value) ?? 0
        parameterPredicted = checkInfinite(predicted)
        parameterLLN = checkInfinite(LLN)
        self.zScore = checkInfinite(zScore)
        calculatePercentValues()
    }
    
    private func checkInfinite(_ value:Float?) -> Float? {
        if value == nil {
            return nil
        }else if (value!.isNaN || value!.isInfinite) {
            return 0
        }else {
            return value
        }
    }
    
    public init?(map: Map){}
    
    mutating public func mapping(map: Map) {
        parameterType <- map["parameterType"]
        parameterValue <- map["parameterValue"]
        parameterPredicted <- map["parameterPredicted"]
        parameterLLN <- map["parameterLLN"]
        zScore <- map["zScore"]
        calculatePercentValues()
    }
    
    private mutating func calculatePercentValues(){
        if parameterPredicted != nil && parameterPredicted != 0 {
            parameterPercent = (parameterValue / parameterPredicted!) * 100
        }

        if parameterPercent > 80 {
            parameterPercentColor =  UIColor.green
            parameterPercentFeedback = "Normal"
        }else if parameterPercent > 40 {
            parameterPercentColor =  UIColor.yellow
            parameterPercentFeedback = "Poor"
        }else {
            parameterPercentColor =  UIColor.red
            parameterPercentFeedback = "Critical"
        }
    }
}

