//
//  ViewController.swift
//  Trial Tab Controller
//
//  Created by Orhun Dündar on 15.01.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import ObjectMapper

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textView: UITextView!
    var measurement:SMeasurement!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        let jsonString = Example().FVCexampleJson1
        
        measurement = (Mapper<SMeasurement>().map(JSONString: jsonString ) ?? nil)!
        
        
        
        
        
        let trialVC = TrialControllerView(buttonCount: measurement.tests.count)
        trialVC.delegate = self
        let vieew = trialVC.createButtons()
        scrollView.addSubview(vieew)
        
        self.scrollView.trailingAnchor.constraint(equalToSystemSpacingAfter: vieew.trailingAnchor, multiplier:  0).isActive = true
        self.scrollView.leadingAnchor.constraint(equalToSystemSpacingAfter: vieew.leadingAnchor, multiplier:  0).isActive = true
        
        
        
    }
    
}

extension ViewController : TrialControllerViewDelegate {
    
    func trialSelected(trialNumber: Int) {
        var s = Mapper<MeasurementTest>.toJSONString(self.measurement.tests[trialNumber-1], prettyPrint: true)
        self.textView.text = s
    }
    
    
}

