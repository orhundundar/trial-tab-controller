//
//  TrialControllerView.swift
//  Trial Tab Controller
//
//  Created by Orhun Dündar on 15.01.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation
import UIKit

protocol TrialControllerViewDelegate {
    func trialSelected(trialNumber:Int)
}

class TrialControllerView {
    
    init(buttonCount:Int) {
        self.buttonCount = buttonCount
    }
    
    var buttonCount = 0
    var buttons:[TabButtonView] = []
    var delegate:TrialControllerViewDelegate!
    
    func createButtons() -> UIView {
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: (buttonCount * 68) + 88, height: 50)
        view.isUserInteractionEnabled = true
        
        for i in 0...buttonCount - 1 {
            
            let button:TabButtonView = TabButtonView(frame: CGRect(x: (i * 68) + 2, y: 4, width: 64, height: 42), isTestAcceptable: true, testNumber: i+1)
            button.delegate = self
            self.buttons.append(button)
            view.addSubview(button)
            
        }
        //For Result Button
        let resultButton:TabButtonView = TabButtonView(frame: CGRect(x: (buttonCount * 68) + 2, y: 4, width: 84, height: 42), isTestAcceptable: true, testNumber: -1)
        resultButton.delegate = self
        self.buttons.append(resultButton)
        view.addSubview(resultButton)
        
        return view
       }
    
    func resetButtons(){
        for button in buttons {
            button.resetButton()
        }
    }
    
}
extension TrialControllerView : TrialButton {
    
    func buttonPressed(count: Int) {
        delegate.trialSelected(trialNumber: count)
        resetButtons()
    }
    
    
}
